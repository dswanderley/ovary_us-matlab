function [ IMOUT ] = acontour( I, PART, varargin )
%ACONTOUR Segment image into foreground and background using active contour.
%   ACONTOUR segment image into based in Chan-Vese active contour method.
%   The function use a list (struct) of particles as masks to produce a
%   more accurate detection of the particles in the image.
%
%       B = ACONTOUR(A, P);
%
%   A : input grayscale image
%   P : struct of particles (particles must contain property 'points'.
%   B : output binary image (black background)
%
%   B = ACONTOUR(A, P, MAX) allows to control the maximum number of 
%   iterations setting input MAX.
%
%   Example:
%   --------
%
%   B = acontour(A, P);
%

%   Author: D. Wanderley, Universidade do Porto


if nargin < 3
    maxIterations = 200;
else
    maxIterations = varargin{1};
end
siz = size(I);
particles = PART;
IMOUT = zeros(siz);
for c = 1:length(particles)
    mask = zeros(siz);
    pos = sub2ind(siz, particles(c).points(:,1), particles(c).points(:,2));
    mask(pos) = 1;
    bw = activecontour(I, mask, maxIterations, 'Chan-Vese');
    IMOUT = IMOUT + double(bw);
end

end

