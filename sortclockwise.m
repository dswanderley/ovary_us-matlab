function [ olist ] = sortclockwise(H, W,  plist)
%SORTCLOCKWISE Sort a sequence of pixel in clockwise.
%   SORTCLOCKWISE is used to sort the pixels of a continue closed sequence 
%   of pixels in clockwise.
%
%   Pixel coordinates must be in global coordinates (see sub2ind).
%
%   OL = SORTCLOCKWISE(H, W, L);
%
%   H : height of the related image or matrix
%   W : width of the related image or matrix
%   L : input vector of points in global coordinates
%   OL: output vector of points in global coordinates
%
%   Example:
%   --------
%   IND = sub2ind([H, W], Y, X);
%   OL = sortclockwise(H, W,  IND);
%

%   Author: D. Wanderley, Universidade do Porto

% Param
siz = [H, W];
% Clear corners and pixels outside circuit
plist = clearcorners(H, W,  plist);
% Auxiliary matrix 
M = zeros(siz);
M(plist) = 1;
% Get first pixel (on top)
[lin, col] = ind2sub(siz,plist);
ptl = min(lin);
topsL = find(lin == ptl); pt = round(length(topsL)/2);
topsC = col(topsL); stc = sort(topsC); ptc = stc(pt);
% Global coords
pTop = sub2ind(siz, ptl, ptc);
% Update list
olist = zeros(size(plist));
olist(1) = pTop;
M(pTop)=0;
% Get second pixel looking clockwise
for k = [8,9,6,7,3]
    [lcenter, ccenter] = ind2sub(siz,pTop);
    [l, c] = ind2sub([3,3],k);
    pos = [lcenter, ccenter] + [l, c] - 2;
    if(M(pos(1),pos(2)) == 1)
        pos = [lcenter, ccenter] + [l, c] - 2;
        olist(2) = sub2ind(siz, pos(1),pos(2));        
        M(pos(1),pos(2)) = 0;        
        break;
    end
end
for n = 2:length(plist)-1
   center = olist(n);
   % Check if has any pixel 
   if center == 0
       break
   end
    [lcenter, ccenter] = ind2sub(siz,center);
    % Run neighborhood on clockwise
    for k = [7,8,9,6,4,1,2,3]
        [l, c] = ind2sub([3,3],k);
        pos = [lcenter, ccenter] + [l, c] - 2;
        % Analyze neighborhood
        if(M(pos(1),pos(2)) == 1)
            olist(n+1) = sub2ind(siz, pos(1),pos(2));
             M(pos(1), pos(2)) = 0;
           % M(lcenter-1:lcenter+1, ccenter-1:ccenter+1) = 0;
           break;
        end
       %imshow(M)
    end    
end
% Ignore unseted positions
olist(olist == 0) = [];
end

