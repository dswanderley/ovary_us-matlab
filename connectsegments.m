function olist = connectsegments(iheight, iwidth,  plist)
%CONNECTSEGMENTS Connects two segments separed by one pixel.
%   CONNECTSEGMENTS return a list of pixel with a unique sequence if it is
%   possible. Edge's pixels coordinates of a region are used as input.
%
%       P = CONNECTSEGMENTS(H, W, L);
%
%   H : matrix height
%   W : matrix width
%   L : input pixels vector in global coordinates
%   P : output pixels vector in global coordinates
%
%   Example:
%   --------
%
%   P = connectsegments(H, W, L);
%

%   Author: D. Wanderley, Universidade do Porto

z = plist;
M = zeros(iheight, iwidth); M(z) = 1;

for c = 1:numel(z)
    % pixel coord
    [lin, col] = ind2sub([iheight, iwidth], z(c));
    
    win = M(lin-2:lin+2,col-2:col+2);
    H = sum(win,1);
    V = sum(win,2);
    
    ll = lin; cc = col;
    
    if (H(1) > 0 && H(3) == 0)
        cc = col-1;
    end
    
    if (H(5) > 0 && H(4) == 0)
        cc = col+1;
    end
    
    if (V(1) > 0 && V(2) == 0)
        ll = lin-1;
    end
    
    if (V(5) > 0 && V(4) == 0)
        ll = lin+1;
    end
    
    M(ll,cc) = 1;
    
end

olist = find(M == 1);

end
