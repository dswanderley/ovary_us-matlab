function [ imgOut ] = invalidateborder ( imgIn )
%INVALIDATEBORDER Set borders of image as background
%   INVALIDATEBORDER is used to set the border os an image as background.
%   Each particle pixel touching a border pixel (invalid) will also be 
%   set as background.
%   
%   Image must be double image, with black background and white particles.
%
%   B = INVALIDATEBORDER(A);
%
%   A : input binary image
%   B : output binary image
%
%   Example:
%   --------
%   BW = double(imbinarize(imread('coins.png'))); BW = BW(10:end,10:end);
%   B = invalidateborder(BW);
%   figure, imshow(BW);  figure, imshow(B);
%

%   Author: D. Wanderley, Universidade do Porto

foreground = 1;
background = 0;
invalid = -1;

[iheight, iwidth] = size(imgIn);
imgOut = imgIn;
% Set Border as invalid
imgOut(1,:) = invalid; imgOut(iheight,:) = invalid; 
imgOut(:,1) = invalid; imgOut(:,iwidth) = invalid;
flag = 0;
while flag == 0
    [pl, pc] = find(imgOut == foreground);
    %%% Pixel windown in global coordinates %%%
    center = iheight*(pc-1) + pl;       %[pl,pc];
    top = iheight*(pc-1) + (pl-1);      %[pl-1,pc];
    down = iheight*(pc-1) + (pl+1);     %[pl+1,pc];
    left = iheight*((pc-1)-1) + pl;     %[pl,pc-1];
    right = iheight*((pc+1)-1) + pl;    %[pl,pc+1];
    %%% Calculate neighbors %%%
    values = [imgOut(center),imgOut(top),imgOut(down),imgOut(left),imgOut(right)];
    %%% Checks for external neighbors %%%
    [lval, ~] = find(values < 0);
    if lval > 0
        imgOut(center(lval)) = -1;
    else
        imgOut(imgOut<0) = background;
        flag = 1;
    end
end

end

