function [ particles ] = findparticles( imIn, varargin )
%FINDPARTICLES Returns a struct with all particles in an image.
%   FINDPARTICLES is used to find particles in a two level double image.
%   The input image is a grayscale image with black background and
%   particles set as white.
%
%       P = FINDPARTICLES(I);
%
%   I : input grayscale image
%   P: output particles: struct of particles with 'points' property.  
%
%   P = FINDPARTICLES(I, MIN) allows to control the minimal size to 
%   classify a particle setting the MIN input. Default MIN = 200 px;
%
%   P = FINDPARTICLES(I, MIN, SCALE) allows to reduce the computional  
%   complexity doing a downscale of the input image I. The results are 
%   based on the original I dimension. Default SCALE = 1.
%
%   Example:
%   --------
%
%   P = findparticles(I);
%

%   Author: D. Wanderley, Universidade do Porto

% Set inicial default values
minimum = 1;    scale = 1;
if nargin > 1,  minimum = varargin{1}; end
if nargin > 2,  scale = varargin{2}; end
% Set image to be process
im1 = imIn;
if scale ~= 1 % rescale image if necessary 
    im1 = imresize(imIn, scale);
end
% Value of pixel particle
partPixelColor = 1;
% Value of checked pixel particle
checkedPixelColor = 0.5;
% Image height
iheight = size(im1, 1);
% Particles stack
jparticles = java.util.Stack();
%%% Loop to analyze each pixel seted as  %%%
flag = 0;
while flag == 0
    part = java.util.Stack();
    stack = java.util.Stack();
    % Start a new particle analyze
    [pl, pc] = find(im1 == partPixelColor);
    point = [pl(1) pc(1)];    
    stack.add(point);
    part.add(point);
    % Set first central pixel as checked
    im1(point(1),point(2)) = checkedPixelColor;
    % Number of pixel in the same region (start with 1)
    count = stack.size;
    %%% Loop to analyze each pixel and define particle limits %%%
    while count > 0
        %%% Set window - Central Pixel %%%
        pcentral = stack.pop();
        %center = iheight*(pcentral(2)-1) + pcentral(1);      %[pl,pc];
        top = iheight*(pcentral(2)-1) + (pcentral(1)-1);      %[pl-1,pc];
        down = iheight*(pcentral(2)-1) + (pcentral(1)+1);     %[pl+1,pc];
        left = iheight*((pcentral(2)-1)-1) + pcentral(1);     %[pl,pc-1];
        right = iheight*((pcentral(2)+1)-1) + pcentral(1);    %[pl,pc+1];
        %%% Top pixel %%%
        if (im1(top) == partPixelColor)
            ptop = [pcentral(1)-1,pcentral(2)];
            stack.push(ptop);
            part.add(ptop);
            im1(ptop(1),ptop(2)) = checkedPixelColor;
        end
        %%% Left pixel %%%
        if (im1(left) == partPixelColor)
            pleft = [pcentral(1),pcentral(2)-1];
            stack.push(pleft);
            part.add(pleft);
            im1(pleft(1),pleft(2)) = checkedPixelColor;
        end
        %%% Right pixel %%%
        if (im1(right) == partPixelColor)
            pright = [pcentral(1),pcentral(2)+1];
            stack.push(pright);
            part.add(pright);
            im1(pright(1),pright(2)) = checkedPixelColor;
        end
        %%% Down pixel %%%
        if (im1(down) == partPixelColor)
            pdown = [pcentral(1)+1,pcentral(2)];
            stack.push(pdown);
            part.add(pdown);
            im1(pdown(1),pdown(2)) = checkedPixelColor;
        end
        % Number of pixel in the same region
        count = stack.size;
    end
    % Ignore small particles
    if (part.size > minimum)
        jparticles.add(part);
    end
    % Test if it is completed
    if (isempty(find(im1 == partPixelColor, 1)))
        flag = 1;
    end
end
% Set MATLAB struct
len1 = jparticles.size;
particles(len1).points = [];
% Convert java stack to MATLAB struct
for c=1:len1
    part = jparticles.pop();
    len2 = part.size;
    points = zeros(len2,2);
    for k=1:len2
        p = part.pop();
        points(k,1) = p(1);
        points(k,2) = p(2);
    end
    % Return to original scale if scale different of 1
    if scale ~= 1 
        M = zeros(size(im1));
        gpos = sub2ind(size(im1), points(:,1), points(:,2));
        M(gpos) = 1;
        M = imresize(M,1/scale);
        [py, px] = find(M == 1);
        points = [py, px];
    end
    % Store points
    particles(c).points = points;
end
% Return a struct of particles with points
end

