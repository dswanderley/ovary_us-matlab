function [ particles ] = sortparticles( particles )
%SORTPARTICLES Sort particles by size
%   SORTPARTICLES is used to sort all PARTICLEs in a struct by size. 
% The PARTICLEs are compared by the number of points contained in 
% propertie POINTS.
%
% Each PARTICLE must contain a propertie points, which is a pair [Y,X],
% with the pixel coordinate.
%
% P = sortparticles(P0);
%
% P0: input struct of particles
% P : output struct of particles
%
% Example:
%
% P0(n).points = [Y, X];
% P = sortparticles(P0);
% M = zeros(MAX_Y, MAX_X);
% pos = ind2sub([MAX_Y, MAX_X],P(1).points(:,1),P(1).points(:,2));
% M(pos) = 1;
% figure, imshow(M);

% Author: D. Wanderley, Universidade do Porto

len = length(particles);

while (len > 0)
    % Iterate through x
    pnew = 0;
    for c = 2:len
        % Swap elements in wrong order
        if (length(particles(c).points) > length(particles(c-1).points))
            particles = swap(particles,c,c - 1);
            pnew = c;
        end
    end
    len = pnew;
end

end


function x = swap(x,m,n)
% Swap x(i) and x(j)
% Note: In practice, x xhould be passed by reference

val = x(m);
x(m) = x(n);
x(n) = val;

end
