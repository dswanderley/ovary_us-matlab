function [ coords ] = majoraxes( imIn )
%SORTCLOCKWISE Search the main axes of a particle in an image
%   SORTCLOCKWISE is used to find the major axis and its principal 
%   orthogonal axis.
%   Image must be binary and contain one particle in white.
%
%   C = MAJORAXES(I);
%
%   I : input binary image with one particle
%   C : output coordinates, struct with two pair of points (p1 and p2).
%       C(1).p1 : [Y11, X11]
%       C(1).p2 : [Y12, X12]
%       C(2).p1 : [Y21, X21]
%       C(2).p2 : [Y22, X22]
%
%   Example:
%   --------
%   I = zeros(100,100);
%   I(35:55,35:55) = 1;
%   C = majoraxes(I);
%

%   Author: D. Wanderley, Universidade do Porto

obj = edge(imIn);

[ed_lin, ed_col] = find(obj == 1);

[y1, y2] = meshgrid(ed_lin);
[x1, x2] = meshgrid(ed_col);

distMatrix = triu((x1-x2).^2 + (y1-y2).^2);
dmax = max(max(distMatrix));
[maxl, maxc] = find(distMatrix == dmax);

p1 = [y1(1,maxc(1)), x1(1,maxc(1))];
p2 = [y2(maxl(1),1), x2(maxl(1),1)];

mask = triu(ones(size(distMatrix))); mask(mask == 0) = NaN; mask(mask == 1) = 0;
thetas = atan((y1-y2)./(x1-x2)) + mask;
theta = thetas(maxl(1),maxc(1));

if (theta == 0)
    [ort_lin, ort_col] =  find(abs(thetas) == pi/2);
    ort =  find(abs(thetas) == pi/2);
elseif (theta == pi/2)
   [ort_lin, ort_col] = find(thetas == 0);
    ort = find(thetas == 0);
else
	beta = theta - sign(theta)*pi/2;
    
    if beta < 0    
        o1 = find(thetas > 1.11*beta);
        o2 = find(thetas < 0.89*beta);
        ort = o1(ismember(o1,o2));
    else
        o1 = find(thetas < 1.01*beta);
        o2 = find(thetas > 0.89*beta); 
        ort = o1(ismember(o1,o2));
    end
    
    [ort_lin, ort_col] = ind2sub(size(thetas), ort);    
end

ort_d = distMatrix(ort);
po = find(ort_d == max(ort_d));

maxol = ort_lin(po);
maxoc = ort_col(po);

po1 = [y1(1,maxoc(1)), x1(1,maxoc(1))];
po2 = [y2(maxol(1),1), x2(maxol(1),1)];


coords(1).p1 = p1;
coords(1).p2 = p2;
coords(2).p1 = po1;
coords(2).p2 = po2;

end

