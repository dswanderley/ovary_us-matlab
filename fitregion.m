function [ particles, varargout ] = fitregion( I, PART, varargin )
%FITREGION Adjusts the limits of a region to better fit to the expected
%   FITREGION is used to grow or fit a region comparing the pixel 
%   neighborhood. FITREGION can use growing region method, snakes active
%   contour method or both methods to fit the desired region.
%
%       PO = FITREGION(I, PI);
%
%   I : input grayscale image
%   PI: input  particles: struct of particles with 'points' property.  
%   PO: output particles: struct of particles with 'points' property.  
%
%   P = FITREGION(I, R, M) allows to control the method used to fit 
%   the region. M may have the following strings:
%       'grow'  Applies the growing region method 
%       'snaks' Applies the snakes active contour method 
%       'both'  Applies the both methods above
%
%   [PO, EI, PI] = FITREGION(I, R) returns an image EI with the edges of 
%   the final region and an image PI with the mask of de final region, 
%   beyond the particles struct PO.
%
%   Example:
%   --------
%
%   [PO, EI, PI] = fitregion(I, R, 'both');
%   figure, imshow(EI);  figure, imshow(PI);
%

%   Author: D. Wanderley, Universidade do Porto

%%% SET PARAMETERS %%%
    % IMAGE SIZER %
[iheight, iwidth] = size(I);
    % PARTICLES %
particles = PART;
    % METHOD %
METHOD = 'grow';
if nargin > 2,   METHOD = varargin{1};   end
    % DEFINE METHOD FLAGS %
switch METHOD
    case 'grow'
        HAS_GROW = true;
        HAS_SNAKES = false;
    case 'snakes'
        HAS_GROW = false;
        HAS_SNAKES = true;
    case 'both'
        HAS_GROW = true;
        HAS_SNAKES = true;
    otherwise
        HAS_GROW = true;
        HAS_SNAKES = true;
end
% SET GROWING REGION PARAMETERS %
if HAS_GROW
    TRESH_VALUE = 0.25;
    NEIG_SIZE = 5;
    if nargin > 3
        [TRESH_VALUE, NEIG_SIZE] = varargin{2};
    end
end
% SET SNAKES PARAMETERS %
if HAS_SNAKES
    % Snake params
    alpha = 0.4; beta = 0.2; gamma = 1; kappa = 0.15;
    wl = 0.3; we = 0.4; wt = 0.7;
    iterations = 200;
    stp = 3; % Step of perimeter
    if nargin == 4
        [alpha, beta, gamma, kappa, wl, we, wt, iterations, stp] = varargin{2};
    elseif nargin > 4
        [alpha, beta, gamma, kappa, wl, we, wt, iterations, stp] = varargin{3};
    end
end
% Morphology
SE = strel('disk',3);
% params to compare results
partIm = zeros(iheight, iwidth);
edgeIm = zeros(iheight, iwidth);
% fn = ceil(sqrt(numel(particles))); fm = floor(sqrt(numel(particles)));
% figure,
%%% START PROCESS %%%
for c = 1:length(particles)
    pImg = zeros(iheight, iwidth);
    pos = sub2ind([iheight, iwidth], particles(c).points(:,1), particles(c).points(:,2));
    pImg(pos) = 1;
    % set other particles pixels with a value higher than edges
    I2 = I;
    for k = 1:length(particles)
        if k ~= c
            pos2 = sub2ind([iheight, iwidth], particles(k).points(:,1), particles(k).points(:,2));
            I2(pos2) = 2;
        end
    end
    imGrow = zeros(iheight, iwidth);
    if HAS_GROW
        % Region Growing
        imGrow = growregion(I2, pos, TRESH_VALUE, NEIG_SIZE);
        imGrow = double(imdilate(imerode(imGrow,SE),SE));
        % Estimate perimeter
        perim = find(bwperim(imGrow)==1);
        perim = sortclockwise(iheight, iwidth, perim);
    else
        % Perimeter of particle
        perim = find(bwperim(pImg)==1);
        perim = sortclockwise(iheight, iwidth, perim);
    end
    imSnake = zeros(iheight, iwidth);
    if HAS_SNAKES
        % Snake
        [ys, xs] = ind2sub([iheight, iwidth], [perim(1:stp:end); perim(1)]);
        % Snake initial coords
        n = length(xs);
        t = 1:n;        ts = 1:0.1:n;    % Interpolates points
        xy = [xs,ys]';  xys = spline(t,xy,ts);
        xs = xys(1,:);
        ys = xys(2,:);
        % Apply SNAKE algorithm
        [snakeY, snakeX] = snake(I2, xs, ys, alpha, beta, gamma, kappa, wl, we, wt, iterations);
        % Connect segments output snake
        snakeP = sub2ind([iheight, iwidth], snakeY, snakeX);
        snakeP = connectsegments(iheight, iwidth,  snakeP);
        % Show output
        imSnake(snakeP) = 1; imSnake = imfill(imSnake);
        edPoints = snakeP;
        fillpoints = find(imSnake == 1);
    else
        edPoints = perim;
        fillpoints = find(imGrow==1);
    end
    % Compare reshape
%     RGB = zeros(iheight, iwidth, 3);
%     RGB(:,:,1) = pImg;
%     RGB(:,:,2) = imGrow;
%     RGB(:,:,3) = imSnake;
%     subplot(fm,fn,c), imshow(RGB)
    % Update particles points
    [posY, posX] = ind2sub([iheight, iwidth], edPoints);
    particles(c).points = [posY, posX];
    partIm(fillpoints) = 1;
    edgeIm(edPoints) = 1;
end

varargout{1} = edgeIm;
varargout{2} = partIm;
%varargout{3} = RGB;

end

