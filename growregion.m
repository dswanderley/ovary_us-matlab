function [ outpixels ] = growregion (I, regionpixels, varargin)
%GROWREGION Grows the limits of an region
%   GROWREGION is used to grow a region comparing the pixel neighborhood 
%   with the average level of the region. GROWREGION returns a imagem with
%   black background and white expanded region.
%
%   P = GROWREGION(I, R);
%
%   I : input grayscale image
%   R : Image with region mask set as white (1)
%   P : output binary image with expansed region
%
%   P = GROWREGION(I, R, T, N) allows to control the threshold T and the 
%   neighborhood N. The default values are T = 0.2 and N = 5;
%
%   Example:
%   --------
%
%   P = growregion(I, R);
%   figure, imshow(I);  figure, imshow(P);
%

%   Author: D. Wanderley, Universidade do Porto

% Verify if threshold window size is set
T = 0.2;
if (nargin > 2), T = varargin{1}; end
% Verify if neighborhood window size is set
neigsize = 5;
if (nargin > 3), neigsize = varargin{2}; end
% set dimensions
[h, w] = size(I);
% pixels of region
if  (size(regionpixels,2) == 2)
    pregion = sub2ind([h, w], regionpixels(:,1), regionpixels(:,2));
elseif (size(regionpixels,1) == 2)
    pregion = sub2ind([h, w], regionpixels(1,:), regionpixels(2,:));
else
    pregion = regionpixels;
end
% Set Image with particle region
region = zeros(h,w);
region(pregion) = 1;
% Window for grow edge
win = [0,-1; -1,0; 1,0; 0,1];
% Window of external neighborhood
[xwin,ywin] = meshgrid(-neigsize:neigsize);
xwin = reshape(xwin, numel(xwin), 1);
ywin = reshape(ywin, numel(ywin), 1);
outwin = [ywin, xwin];
% Flags to stop iteration
count = 0;
flag = false;
% Start itarations (stop if has no changes - flag = true)
while ~flag && count < 20
    % Update region mean
    me = mean(I(pregion));
    M = region;
    % Calculate region perimeter
    perim = find(bwperim(M)==1);
    % Run all positions over perimeter
    for c = 1:numel(perim)
        % Edge pixel position
        [crow, ccol] = ind2sub([h, w], perim(c));
        % Edge pixels neighborhood
        winrow = win(:,1) + crow;
        wincol = win(:,2) + ccol;
        pwin = sub2ind([h, w], winrow, wincol);
        % Get pixels on neighborhood outside the region
        members = ismember(pwin, pregion);
        non_members = pwin(~members);
        % neighborhood
        for k = 1:length(non_members)
            outpos = non_members(k);
            [outrow, outcol] = ind2sub([h, w], outpos);
            owinrow = outwin(:,1) + outrow;
            iwincol = outwin(:,2) + outcol;
            owin = sub2ind([h, w], owinrow, iwincol);
            % Get pixels on neighborhood outside the region
            outsiders = ~ismember(owin, pregion);
%  if ~(I(owin(outsiders)) == 1)
            % Calculate mean of outsider pixels in the window
            outmean = sum(I(owin(outsiders)))/sum(outsiders);
            % Verify difference and pixel to region
            if(abs(outmean - me) < T)
                M(outpos) = 1;
            end
% end
        end
    end
    % Check if M and regions are the same particle
    if sum(sum(M-region)) == 0
        flag = true;
    else
        region = M;
        pregion = find(M == 1);
    end
    count = count + 1;
end

outpixels = region;

end
