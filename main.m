close all; clear; clc;

%%%%%% PARAMETERS %%%%%%%
MIN_SIZE = 200; % pixel area

im = imread('img_03.png'); %im = imresize(im,0.5);
im1 = rgb2gray(im2double(im));
[iheight, iwidth] = size(im1);
%figure, imshow(im1)

%%%%%%%%%%%%%%%%%%%%%%
%%% Speckle filter %%%
%%%%%%%%%%%%%%%%%%%%%%
% filter parameters
sigmar = 40;
eps    = 1e-3;
% Gaussian bilateral filter
sigmas = 3;
[im2,Ng] = imfilterbilateral(im1, sigmar, sigmas, eps, 'Gauss');

%%%%%%%%%%%%%%%%%%%
%%% Enhancement %%%
%%%%%%%%%%%%%%%%%%%
im2e = adapthisteq(im2);  % CLAHE
% figure, imshow(im2e);

%%%%%%%%%%%%%%%%%%%%
%%% Segmentation %%%
%%%%%%%%%%%%%%%%%%%%

%%% Threshold %%%
threshold = mean(mean(im2e)); % Global threshold
im3 = imbinarize(im2e, threshold);
% T5 = adaptthresh(im2e,'ForegroundPolarity','dark');
% im3 = imbinarize(im2e,T5);

% Set border pixel as background
im3 = -(im3-1);
% Morphology
se5 = strel('disk',5);
se3 = strel('diamond',3);
im4 = imerode(imerode(im3,se5),se5);
im4 = imdilate(im4,se5);
% figure, imshow(im4);

%%% Locate and isolate particles %%%
particles = sortparticles(findparticles(invalidateborder(im4), MIN_SIZE/4, 0.5));
%%% Active contour process %%%
im5 =  acontour(im2e, particles, 200);
% Morphology
se = strel('disk',3);
im5 = imdilate(imerode(im5,se),se);
%%% Find particles after active contour %%%
particles = sortparticles(findparticles(invalidateborder(im5), MIN_SIZE));
%%% Adjust follicles %%%
[oparticles, edgeIm, partIm] = fitregion(im2e, particles);
% oparticles = fitregion(im2e, particles,'both');
% oparticles = fitregion(im2e, particles, 'snakes');

%%%%%%%%%%%%%%%%%%%%
%%% Show Results %%%
%%%%%%%%%%%%%%%%%%%%
coords(length(oparticles)).p1=[];
figure, imshow(edgeIm+im1)
hold on
for c = 1:length(oparticles)
    mask = zeros(iheight, iwidth);
    pos = sub2ind([iheight, iwidth], oparticles(c).points(:,1), oparticles(c).points(:,2));
    mask(pos) = 1;
    cd = majoraxes(mask);
    coords(c).r1 = cd(1);
    coords(c).r2 = cd(2);
    plot(coords(c).r1.p1(2), coords(c).r1.p1(1), '*c');
    plot(coords(c).r1.p2(2), coords(c).r1.p2(1), '*c');
    plot(coords(c).r2.p1(2), coords(c).r2.p1(1), '*y');
    plot(coords(c).r2.p2(2), coords(c).r2.p2(1), '*y');
end
hold off
