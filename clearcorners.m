function [ olist ] = clearcorners( h, w,  plist )
%CLEARCORNERS Delete excess pixels in the corners.
%   CLEARCORNERS return a list of pixel without redundant pixels in corner
%   edges. The edge's pixels coordinates of a region are used as input.
%
%       P = CLEARCORNERS(H, W, L);
%
%   H : matrix height
%   W : matrix width
%   L : input pixels vector in global coordinates
%   P : output pixels vector in global coordinates
%
%   Example:
%   --------
%
%   P = clearcorners(H, W, L);
%

%   Author: D. Wanderley, Universidade do Porto

% params
siz = [h,w];
% auxiliar matrix
M = zeros(siz);
M(plist) = 1;
M = preventring(M);
% window and filter for first iteration
win = [-1,-1; -1,0; -1,1; 0,-1; 0,0; 0,1; 1,-1; 1,0; 1,1];
fil = [1; 1; 1; 1; 0; 1; 1; 1; 1];
% Delete pixels without center of continuous line
for c = 1:length(plist)
    center = plist(c);
    [lcenter, ccenter] = ind2sub(siz,center);
    pos = sub2ind(siz, win(:,1)+lcenter, win(:,2)+ ccenter);
    aux = M(pos) .* fil;
    % Verify if pixel has not continuous
    if sum(aux) == 2
        % Test distance
        aux2 = find(aux == 1);
        [ys, xs] = ind2sub([3,3],aux2);
        d = ((ys(1) - ys(2))^2 + (xs(1) - xs(2))^2)^0.5;
        if d <= 1
            M(center) = 0;
        end
    end
end
% Refresh list
plist = find(M==1);
% Analyze each pixel on list
for k = 1:length(plist)
    % Center pixel
    center = plist(k);
    [lcenter, ccenter] = ind2sub(siz,center);
    % Get diagonal neighbors
    q1 = [M(lcenter-1,ccenter), M(lcenter,ccenter+1)];
    q2 = [M(lcenter,ccenter+1), M(lcenter+1,ccenter)];
    q3 = [M(lcenter+1,ccenter), M(lcenter,ccenter-1)];
    q4 = [M(lcenter,ccenter-1), M(lcenter-1,ccenter)];
    % Clear pixel with more than one diagonal neighbor
    if sum(q1) > 1 || sum(q2) > 1 || sum(q3) > 1 || sum(q4) > 1
        M(center) = 0;
    end
end
% output
olist = find(M==1);
end

function oM = preventring(M)
%PREVENTRING Selects a direction if a small ring can happen
%   Detailed explanation goes here

HV = [0,1,0;1,0,1;0,1,0]/4;
M1 = imfilter(M, HV);
oM = M;
[posY, posX] = find(M1==1);
if (posY)
    % for each incidence
    for k = 1:length(posY)
        pos = [posY(k)-1, posX(k); 
            posY(k), posX(k)-1; posY(k), posX(k)+1; posY(k)+1, posX(k)];
        neig=[-1,-1; -1,0; -1,1;  0,-1;  0,0;  0,1; 1,-1;  1,0;  1,1];
        weights = zeros(4,1);
        % Verifies the amount of pixels in each neighborhood
        for c = 1:4
            pyx = pos(c,:);
            npos = sub2ind(size(M),pyx(1)+neig(:,1),pyx(2)+neig(:,2));
            weights(c) = sum(M(npos));
        end
        gPos = sub2ind(size(M),pos(:,1),pos(:,2));
        oM(gPos) = weights == max(weights);
    end
end
end
